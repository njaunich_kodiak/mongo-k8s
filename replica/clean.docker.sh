#!/bin/sh

. scripts_setup.sh
docker container stop $DOCKER_CONTAINER_NAME
docker rm $DOCKER_CONTAINER_NAME
