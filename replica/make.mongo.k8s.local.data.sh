#!/bin/sh

. scripts_setup.sh
echo "Creating $MONGO_NODES_NUMBER MongoDB data directories"
minikube ssh -- sudo mkdir /data/mongo-k8s
for ((i=0; i<$(($MONGO_NODES_NUMBER + 0)); i++))
do
	echo "Directory for node $i..."
    minikube ssh -- sudo mkdir /data/mongo-k8s/node$(($i+0))
done