#!/bin/sh

# To run this file do '. scripts_setup.sh' in your current shell

export PROJECT_NAME="mongo-k8s"
export DOCKER_CONTAINER_NAME="mongo-k8s"
export REPOSITORY_ID="njaunich_ssl/mongo-k8s"
export ENVIRONMENT="stage"

export MONGO_NODES_NUMBER=3
