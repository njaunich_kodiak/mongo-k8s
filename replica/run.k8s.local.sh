#!/bin/sh

. scripts_setup.sh

export MINIKUBE_CURRENT_STATUS="$(minikube status | grep 'minikube: Running')"
if test "$MINIKUBE_CURRENT_STATUS" != "minikube: Running";
then
    minikube start
    . make.mongo.k8s.local.data.sh
else
    echo $MINIKUBE_CURRENT_STATUS
    . make.mongo.k8s.local.data.sh
fi

eval $(minikube docker-env)

docker build --no-cache -t $PROJECT_NAME:1 .
kubectl create -f kubernetes/