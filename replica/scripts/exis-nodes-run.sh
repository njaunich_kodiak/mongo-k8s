#!/usr/bin/env bash

echo "Starting $1 Exis node containers"
for ((i=0; i<$(($1 + 0)); i++))
do
	echo "$var"
    docker run -d --restart=on-failure -e TCP_PORTS=8000 -p 900$i:8000 --name=mongodb-kubernetes-$(($i+1)) -t mongodb-kubernetes
done
