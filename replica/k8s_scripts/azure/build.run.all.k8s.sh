#!/bin/sh

. scripts_setup.sh

. make.k8s.secrets.sh

docker login -u $APP_ID -p $APP_SECRET_PASSWORD $CURRENT_STRATUS_DOCKER_REGISTRY
docker build --no-cache -t $PROJECT_NAME:1 ../../
docker tag $PROJECT_NAME:1 $CURRENT_STRATUS_DOCKER_REGISTRY/$PROJECT_NAME:1
docker push $CURRENT_STRATUS_DOCKER_REGISTRY/$PROJECT_NAME:1

. make.k8s.data.sh
sleep 10

kubectl create -f ../../kubernetes/azure/