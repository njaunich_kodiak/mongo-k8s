#!/bin/sh

# To run this file do '. scripts_setup.sh' in your current shell

export PROJECT_NAME="mongo-k8s"
export DOCKER_CONTAINER_NAME="mongo-k8s"
export REPOSITORY_ID="njaunich_sss/mongo-k8s"
export ENVIRONMENT="stage"

export K8S_NODES_NUMBER=1

export KUBECONFIG=../../kubernetes/kubeconfig/azure.json

export CURRENT_STRATUS_DOCKER_REGISTRY="stratusdockerregistry001.azurecr.io"
export APP_ID="3fc44304-5066-4204-bbac-feecc9d2dbf5"
export APP_SECRET_PASSWORD="52401101-1cf0-4fcb-af82-e6371865b1c9"
export EMAIL="nicholas.jaunich@stratussilverlining.com"