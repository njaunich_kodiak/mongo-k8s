. scripts_setup.sh

export MINIKUBE_CURRENT_STATUS="$(minikube status | grep 'minikube: Running')"
if test "$MINIKUBE_CURRENT_STATUS" != "minikube: Running";
then
	minikube start
    minikube ssh -- sudo rm -rf /data/mongo-k8s
else
    echo $MINIKUBE_CURRENT_STATUS
    minikube ssh -- sudo rm -rf /data/mongo-k8s
fi

echo "Removed directory /data/mongo-k8s"