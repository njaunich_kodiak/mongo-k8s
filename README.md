# mongo-k8s
Deploy a mongo cluster on kubernetes. 
Based on [Enabling Microservices: Containers & Orchestration Explained](https://www.mongodb.com/collateral/microservices-containers-and-orchestration-explained)

# MongoDB replica set

`replica/kubernetes/azure/*.yml` creates 3 mongodb replication controllers and their corresponding services. The 3 mongodb instances can consist a replica set named as "mongo-replica-rc". 

One of mongodb replication controller's yaml:
```
apiVersion: v1
kind: ReplicationController
metadata:
  name: mongo-replica-rc0
  labels:
    name: mongo-replica-rc
spec:
  replicas: 1
  selector:
    name: mongo-replica-node0
  template:
    metadata:
      labels:
        name: mongo-replica-node0
    spec:
      containers:
      - name: mongo-replica-node0
        image: hyge/mongo-cluster
        env:
          - name: mongo_node_name
            value: mongo-replica-node
          - name: mongo_nodes_number
            value: "3"
          - name: mongo_replica_set_name
            value: my_replica_set
        ports:
        - containerPort: 27017
        volumeMounts:
        - name: mongo-replica-storage0
          mountPath: /data/db
      volumes:
      - name: mongo-replica-storage0
        emptyDir: {}
```
* To scale nodes of the mongo replica, promote the environmental variable "mongo_nodes_number". 
* Environmental variable "mongo_replica_set_name" is the name of replica set.
* Environmental variable "mongo_node_name" is the name prefix for each node. The suffix of the name will be "Nth" number. It must match k8s svc's name.
* See `replica/start_replica.sh` and `replica/Dockerfile` for more details.

One mongo service yaml:
```
apiVersion: v1
kind: Service
metadata:
  name: mongo-replica-node-0
  labels:
    name: mongo-svc
spec:
  clusterIP: None
  ports:
  - port: 27017
    targetPort: 27017
    protocol: TCP
    name: mongo-svc-port
  selector:
    name: mongo-replica-node0
```

# See azure_kubernetes repo for information on how the scripts below were created
[azure_kuberentes](https://bitbucket.org/njaunich_sss/azure_kubernetes)

## How to deploy a replica set to Stratus Azure Kubernetes

To start a 3 node mongo rs, run 
`cd replica/k8s_scripts/azure`

If codebase has been updated/docker container is out of date do
`./build.run.k8s.sh`

Otherwise
`./run.k8s.sh`

## To remove mongo-k8s nodes/services (but NOT external service or DB data) from Stratus Azure Kubernetes
`cd replica/k8s_scripts/azure`

`./clean.k8s.sh`

## To remove mongo-k8s DB data from Stratus Azure Kubernetes
`cd replica/k8s_scripts/azure`

`./clean.k8s.data.sh`

## How to setup root user for Mongo

First set env vars (for ease of use)
`STRATUS_MONGO_IP=52.175.255.222`

Login to DB
`mongo $STRATUS_MONGO_IP/stratuscloud`

Set DB to use
`use stratuscloud`

Add root user
`db.createUser({user:"root",pwd:"Mobilenerd@1",roles:["readWrite","dbAdmin"]});`


#### Use driver to connect to mongo cluster

For example: node.js
```
var connectionString = 'mongodb://mongo-replica-svc-a:27017,mongo-replica-svc-b:27017,mongo-replica-svc-c:27017/your_db?replicaSet=my_replica_set' +

MongoClient.connect(connectionString, callback)
```

